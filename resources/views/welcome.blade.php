@extends('base.base')

@section('title', 'Restaurant - menu')

@section('content')
    <div class="row m-4">
        <h4 class="text-center" style="color: blue">Menu</h4>
        <div class="col-md-6">
            <div class="mb-3 text-center" style="border: 1px solid black;color: blue">
                Food
            </div>
            @foreach ( $foodsMenu as $foodMenu)
                <div class="card mb-3 primary-bg">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src='storage/{{$foodMenu->image}}' class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body row">
                                <div class="col-md-6">
                                    <h5 class="card-title bold">{{$foodMenu->name}}</h5>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title price-label-card">${{$foodMenu->price}}</h5>
                                </div>
                                <p class="card-text">{{$foodMenu->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-6">
            <div class="mb-3 text-center" style="border: 1px solid black;color: blue">
                Beverage
            </div>
            @foreach ( $beveragesMenu as $beverageMenu)
                <div class="card mb-3 primary-bg">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src='storage/{{$beverageMenu->image}}' class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body row">
                                <div class="col-md-6">
                                    <h5 class="card-title bold">{{$beverageMenu->name}}</h5>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title price-label-card">${{$beverageMenu->price}}</h5>
                                </div>
                                <p class="card-text">{{$beverageMenu->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
