<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'category_id',
        'image',
        'price'
    ];
    public function Category() {
        return $this->belongsTo(Category::class);
    }

    use HasFactory;
}
