<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::with('category')->get();
        return ([
            'status'=>'OK',
            'message'=>'Menus has getted',
            "Data"=>$menus
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = $request->file('img')->store('public/img');

        $menu = new Menu;
        $menu->name = $request->input('name');
        $menu->description = $request->input('description');
        $menu->image = substr($path, strlen('public/'));
        $menu->price = $request->input('price');
        $menu->category_id = $request->input('category');
        if ($menu->save()) {
            return response()->json([
                'status'=>'OK',
                'message'=>'Create Success',
                "menuId"=>$menu->id,
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show($menuId)
    {
        $menu = Menu::where('id', $menuId)->get();
        if (!$menu->isEmpty()) {
            return [
                'status'=>'OK',
                'message'=>'menu has getted',
                'data'=>$menu
            ];
        }
        return response()->json([
            'status'=>'ERROR',
            'message'=>'Id not found',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $menuId)
    {
        $menu = Menu::where('id', $menuId)->get();

        if (!$menu->isEmpty()) {
            $menu->name = $request->input('name');
            $menu->description = $request->input('description');
            $menu->image = $request->file('img')->store('public/img');
            $menu->price = $request->input('price');
            $menu->category_id = $request->input('category');
            $menu->update();
            return [
                'status'=>'OK',
                'message'=>'Update Success',
                "menuId"=>$menu->id
            ];
        }
        return response()->json([
            'status'=>'ERROR',
            'message'=>'Id not found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($menuId)
    {
        $menu = Menu::where('id', $menuId)->get();
        if (!$menu->isEmpty()) {
            if ($menu->delete()) {
                return [
                    'status'=>'OK',
                    'message'=>'Delete success',
                    'menuId'=>$menu->id
                ];
            }
        }
        return response()->json([
            'status'=>'ERRPR',
            'message'=>'Id not found'
        ], 404);
    }
}
