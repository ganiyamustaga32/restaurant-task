<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Category;

class HomeController extends Controller
{
    function index() {
        $foodCategory = Category::where('name', 'Food')->first();
        $beverageCategory = Category::where('name', 'Beverage')->first();

        $foodsMenu = Menu::where('category_id', optional($foodCategory)->id)->get();
        $beveragesMenu = Menu::where('category_id', optional($beverageCategory)->id)->get();

        return view('welcome', compact('foodsMenu', 'beveragesMenu', 'foodCategory'));
    }
}
