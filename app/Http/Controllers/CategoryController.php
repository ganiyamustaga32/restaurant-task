<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        return ([
            'status'=>'OK',
            'message'=>'Category has getted',
            'data'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $request->validate();
        $category = new Category();
        $category->name = $request->input('name');
        $category->save();
        return ([
            'status'=>'OK',
            'message'=>'Create Success',
            'categoryId'=>$category->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($categoryId)
    {
        $category = Category::where('id', $categoryId)->get();
        if (!$category->isEmpty()) {
            return ([
                'status'=>'OK',
                'message'=>'Category has getted',
                'data'=>$category
            ]);
        }
        return response()->json([
            'status'=>'ERROR',
            'message'=>'Id not found'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoryId)
    {
        $category = Category::findorFail($categoryId);
        $category->name = $request->input('name');
        $category->update();
        return ([
            'status'=>'OK',
            'message'=>'Update Success',
            'categoryId'=>$category->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryId)
    {
        $category = Category::findorFail($categoryId);
        if ($category->delete()) {
            return ([
                'status'=>'OK',
                'message'=>'Delete success',
                'menuId'=>$category->id
            ]);
        }
    }
}
