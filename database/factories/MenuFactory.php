<?php

namespace Database\Factories;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Menu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id'=>1,
            'name'=>'Martabak Classic',
            'description'=>$this->faker->sentence(),
            'image'=>'img/Klasik.jpg',
            'price'=>$this->faker->randomFloat(2,5,100)
        ];
    }
}
